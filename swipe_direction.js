class Swipe {
    /*
        The constructer that sets the element the swipe should be performed on
        and sets the starting coordinates of the swipe.
    */
    constructor(element) {
        this.xDown = null;
        this.yDown = null;
        this.element = typeof(element) === 'string' ? document.querySelector(element) : element;

        this.element.addEventListener('touchstart', function(evt) {
            this.xDown = evt.touches[0].clientX;
            this.yDown = evt.touches[0].clientY;
        }.bind(this), false);
    }


    /*
        The functions that execute the passed callback if called because of a swipe
        in the regarding direction. 
    */
    onLeft(callback) {
        this.onLeft = callback;

        return this;
    }

    onRight(callback) {
        this.onRight = callback;

        return this;
    }

    onUp(callback) {
        this.onUp = callback;

        return this;
    }
    
    onDown(callback) {
        this.onDown = callback;

        return this;
    }


    /*
        The function that will call the correct on'Direction' function based
        on the direction of the performed swipe. 
    */
    handleTouchMove(evt) {
        if ( ! this.xDown || ! this.yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;
        var yUp = evt.touches[0].clientY;

        this.xDiff = this.xDown - xUp;
        this.yDiff = this.yDown - yUp;

        if ( Math.abs( this.xDiff ) > Math.abs( this.yDiff ) ) { // If the horizontal movement is bigger than the vertical (whether the values are positive or negative doesn't matter here).
            if ( this.xDiff > 0 ) {
                this.onLeft();
            } else {
                this.onRight();
            }
        } else {
            if ( this.yDiff > 0 ) {
                this.onUp();
            } else {
                this.onDown();
            }
        }

        // Reset values.
        this.xDown = null;
        this.yDown = null;
    }

    /*
        The function that, when called, will check if the user actually performs 
        a swipe instead of just a touch and trigger the upper function if so.
    */
    run() {
        this.element.addEventListener('touchmove', function(evt) {
            this.handleTouchMove(evt);
        }.bind(this), false);
    }
}