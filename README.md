This project is to demonstrate how to use the library swipe_direction.nl.
Click on one of the direction buttons and swipe in that direction within the black box
to get an alert.

Tested in mobile view using the DevTools within Chrome.

With this library, you can put custom callbacks on specific swipe events.

To use it, first you need to create a swipe object, passing through the element the
swipe should be performed on.
    - var swiper = new Swipe(document.getElementById('swipeArea'));

Then specify your callback within the function for the right swiper.on'Direction' 
function, depending on what swipe direction you want for the callback.
    - swiper.onLeft(function() { alert('You swiped left.') });

Then check if the user actually performs a swipe instead of a touch and if so, trigger        
the function that will determine the direction of the performed swipe. Based on this 
direction, the correct on'Direction' function will be executed, which will 
execute the given callback for the given direction.
    - swiper.run();